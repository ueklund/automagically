from django.db import models
import signals.models
import Queue
import urllib2

PLUGIN_NAME = 'httpreq'

debug = False

workQueue = Queue.Queue()

def signalHandler(signal):
    if signal.content == 'terminate':
        workQueue.put(None)        

    elif signal.content.startswith('httpreq,do:'):
        workQueue.put(signal.content[11:].strip())

def threadFunc():
    keepRunning = True
    while(keepRunning):
        try:
            s = workQueue.get()

            if s == None:
                print 'Got a none from httpreq workQueue'
                workQueue.task_done()
                keepRunning = False
                continue

            if not s.startswith('http://'):
                s = 'http://' + s

            if debug:
                print 'HTTP REQ plugin: URL', s


            u = urllib2.urlopen(s)
            if debug:
                print u.read()                

            workQueue.task_done()

        except:
            print 'Error executing http request command'

def init():
    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)
