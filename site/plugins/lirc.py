import signals.models
import socket
import sys
import threading
import re
from django.conf import settings

PLUGIN_NAME = 'lirc'

debug = False
lirc_socket_address = '/var/run/lirc/lircd'

# workQueue = Queue.Queue()

ret=False
data=[]
fd=None
sock=None
reply_event=None
send_lock=None
keepRunning=True

def send_command(command):
  global ret, data, fd, sock, reply_event, send_lock, keepRunning
  send_lock.acquire()
  reply_event.clear()
  print>>sys.stderr, "Lircd cmd: '%s'" % (command)
  sock.sendall(command+'\n')
  reply_event.wait()
  ret=(ret, data)
  send_lock.release()
  return ret

def send_ir_signal(remote, button, repeat):
  return send_command('SEND_ONCE %s %s' % (remote,  button))

def close():
  global ret, data, fd, sock, reply_event, send_lock, keepRunning
  sock.close()

def readline():
  global ret, data, fd, sock, reply_event, send_lock, keepRunning
  line=fd.readline()[:-1]
  print>>sys.stderr, "Lircd reply line: %s" % (line)
  return line

def signalHandler(signal):
    global ret, data, fd, sock, reply_event, send_lock, keepRunning
    if signal.content == 'terminate':
        keepRunning=False   
    else :
      print>>sys.stderr, "Signal: '%s'" % (signal.content)
      send_once_re=re.compile('^lirc,send-once:([^,]+),([^,]+)(,([^,]+))?$')
      m=send_once_re.match(signal.content.strip())
      if m : 
        (remote, button, repeat) = m.group(1,2,4)
        # if repeat=None:
        repeat='0'
        send_ir_signal(remote,button,repeat) 

def threadFunc():
    global ret, data, fd, sock, reply_event, send_lock, keepRunning
    while keepRunning:
        line=readline()
        if line=='BEGIN' :
          cmd=readline()
          if cmd=='SIGHUP' :
            print>>sys.stderr, "Lircd got SIGHUP"
            readline() # Check for END
          else :  
            ret=readline()=='SUCCESS'
            data=[]
            if readline()=='DATA':
              n=int(readline())
              while n:
                data.append(readline())
                n=n-1
              readline() # Check for END
          reply_event.set()
        # Store ret and data
        else :
          [code, repeat, button, remote] = line.split(' ', 4)
          print>>sys.stderr, "Lircd remote command: remote=%s button=%s repeat=%s code=%s" % (remote, button, repeat, code)
          if repeat=="00" :
            print "Lircd remote command: remote=%s button=%s -> Send Signal" % (remote, button)        
            signals.models.postToQueue("lirc,ir-signal,remote:%s,button:%s" % (remote,button), PLUGIN_NAME)


def init():
    global ret, data, fd, sock, reply_event, send_lock, keepRunning
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    reply_event=threading.Event()
    send_lock=threading.Lock()
    try:
      sock.connect(lirc_socket_address)
      fd=sock.makefile()
      print 'fd setup'
    except socket.error, msg:
      print >>sys.stderr, msg
      sys.exit(1)

    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)
