from django.db import models
from settings.models import updateSettings, deleteObsoleteGroups
from signals.models import SignalObject

import threading
import sys
import os
import time

PLUGINS_PATH = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], 'plugins')

workers = []
threads = []


def registerSignalWorker(prefix, func, moduleName, thread):
    print 'Worker registered for prefix', '"' + prefix + '"', func
    workers.append((prefix, func, moduleName, thread))


def processSignal(s):
    for prefix, handler, moduleName, thread in workers:
        if s.content.startswith(prefix):
            handler(s)

def alertSettingsChanged(module):
    signal = SignalObject('configuration,changed', 'system', 0)
    for prefix, handler, moduleName, thread in workers:
        if moduleName == module:
            print 'Alert plugin of config changes'
            handler(signal)
            break


class PluginThread(threading.Thread):
    def __init__(self, plugin, func):
        self.func = func
        self.plugin = plugin
        threading.Thread.__init__(self)

    def run(self):
        print 'Thread for', self.plugin, 'started'
        self.func()
        print 'Plugin threadfunction done'



def startThreads():
    print 'About to start plugin threads'
    for item in os.listdir(PLUGINS_PATH):
        if os.path.isfile(os.path.join(PLUGINS_PATH, item)):
            if item.lower().endswith('.py') and item != 'plugins.py':
                moduleName = item[:-3]
                print 'Found plugin', moduleName, '----------------'
                try:
                    prefix, settings, workerFunc, threadFunc = __import__(moduleName, globals()).init()
                    if settings:
                        updateSettings(moduleName, settings)

                    if threadFunc:
                        t = PluginThread(moduleName, threadFunc)
                        t.start()
                    else:
                        t = None

                    if workerFunc and prefix != None:
                        registerSignalWorker(prefix, workerFunc, moduleName, t)


                    print 'Plugin init done --------------------'
                except:
                    print 'Failed initilize plugin'
                    raise

    print 'Time to clean out not used settings'
    deleteObsoleteGroups()

def stopThreads():    
    for prefix, handler, moduleName, thread in workers:
        if thread:
            print 'Closing thread', moduleName
            handler(SignalObject('terminate', 'system'))
            thread.join()
            print 'Close done'
        
    
