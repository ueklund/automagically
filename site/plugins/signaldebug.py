from django.db import models
import signals.models
from settings.models import getSettings
import time
import os
import socket
import select
from curses.ascii import isprint
import datetime
import Queue

PLUGIN_NAME = os.path.splitext(os.path.split(__file__)[1])[0] #This will be the filename without .py

debug = False

global currentSettings
currentSettings = {}
workQueue = Queue.Queue()

def signalHandler(signal):
    global currentSettings

    if not currentSettings or signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        print 'Signaldebug got configuration updated message'
        currentSettings = getSettings(PLUGIN_NAME)
        try:
            fd = open(os.path.join(os.path.split(os.path.abspath(__file__))[0], 'signaldebug.txt'), 'w')
            fd.close()
        except:
            print 'Error trying to reset content of signaldebug.txt'

    if signal.content == 'terminate':
        workQueue.put(None)



    workQueue.put(signal)

def threadFunc():

    currentSettings = getSettings(PLUGIN_NAME)

    while 1:
        s = workQueue.get()
        try:


            if s == None:
                print 'Got a none from signal debug workQueue'
                workQueue.task_done()
                break

            if currentSettings.get('Debug to file', False):

                try:
                    fd = open(os.path.join(os.path.split(os.path.abspath(__file__))[0], 'signaldebug.txt'), 'a')
                    fd.write('%s#%s\n' %(datetime.datetime.now().isoformat(' '), str(s)))
                    fd.close()
                except:
                    if debug:
                        print 'error in signaldebug when writing to file'


        except:
            if debug:
                print 'error in signaldebug'

        workQueue.task_done()                


def init():
    settings = {'Debug to file':   ('bool', False)}

    return ('', settings, signalHandler, threadFunc)

